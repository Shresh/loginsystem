package com.iims.midterm.loginsystem.loginsystem;

import com.iims.midterm.loginsystem.loginsystem.entity.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.iims.midterm.loginsystem.loginsystem.connection.ConnectionFactory.getConnection;

@WebServlet(name = "loginServlet", value = "/login")
public class LoginServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//        System.out.println("helloWorld");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Users user = new Users();

        String name =request.getParameter("username");
        String password = request.getParameter("password");

        try {
            String query = "SELECT * FROM users WHERE username= ?";
            PreparedStatement preparedStatement =getConnection().prepareStatement(query);
            preparedStatement.setString(1,name);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()){
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                user.setFirstname(resultSet.getString("first_name"));
                user.setLastname(resultSet.getString("last_name"));

                if(user.getPassword().equals(password)){

                    out.println("First Name : "+ user.getFirstname() + "    " );
                    out.println("Last Name : "+ user.getLastname() );

                }else{
                    out.println("Invalid Credintials.");
                }
            }else{
                out.println("Sorry, you are not registered.");
            }
        } catch (Exception e) {
//            out.println("THERE MAY NOT BE A USER UNDER THIS USERNAME. PLEASE RECHECK AND TRY AGAIN");
            out.println(e);
        }

    }
}
