<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
    <title>Log In</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div class="form-v7">
    <div class="page-content">
        <div class="form-v7-content">
            <div class="form-left">
                <img src="assets/images/form-v7.jpg" alt="form">
                <p class="text-1">Log In</p>
                <p class="text-2">Privacy policy & Term of service</p>
            </div>
            <div class="form-detail" id="myform">
                <form action="login" method="post">
                    <div class="form-row">
                        <label for="username">USERNAME</label>
                        <input type="text" name="username" id="username" class="input-text">
                    </div>

                    <div class="form-row">
                        <label for="password">PASSWORD</label>
                        <input type="password" name="password" id="password" class="input-text" required>
                    </div>
                    <div class="form-row-last">
                        <input type="submit" name="register" class="register" value="LogIn">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
</div>
</body>
</html>